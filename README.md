pwsafe
======

A Rust library for reading and writing [Password Safe](https://www.pwsafe.org/) databases.
