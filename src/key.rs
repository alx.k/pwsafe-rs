use sha2::{Digest, Sha256};
use twofish::block_cipher_trait::generic_array::typenum::U32;
use twofish::block_cipher_trait::generic_array::GenericArray;

/// Returns ECB key generated from password using key stretching algorithm.
pub fn hash_password(salt: &[u8], iter: u32, password: &[u8]) -> GenericArray<u8, U32> {
    let mut hasher = Sha256::default();
    hasher.input(password);
    hasher.input(&salt);
    let mut key = hasher.result();
    for _ in 0..iter {
        let mut hasher = Sha256::default();
        hasher.input(&key);
        key = hasher.result();
    }
    key
}
